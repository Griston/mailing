<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Doctrine\Mail;

use Skadmin\Mailing\Model\MailTemplateParameter;
use SkadminUtils\DoctrineTraits\Entity;

/**
 * Class Role
 *
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class MailTemplate
{
    use Entity\Id;
    use Entity\Name;
    use Entity\LastUpdate;

    // COLUMNS
    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $subject = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="array")
     * @var array<string>|bool
     */
    private $recipients = [];

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $preheader = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="text")
     * @var string
     */
    private $content = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $type = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $class = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="array")
     * @var mixed[]
     */
    private $parameters = [];

    public function getSubject() : string
    {
        return $this->subject;
    }

    /**
     * @return array<string>|string
     */
    public function getRecipients(?string $implodeBy = null)
    {
        $recipients = is_bool($this->recipients) ? [] : $this->recipients;

        if ($implodeBy === null) {
            return $recipients;
        }

        return implode($implodeBy, $recipients);
    }

    public function getPreheader() : string
    {
        return $this->preheader;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function getType() : string
    {
        return $this->type;
    }

    public function getClass() : string
    {
        return $this->class;
    }

    /**
     * @return MailTemplateParameter[]
     */
    public function getParameters() : array
    {
        $parameters = [];
        foreach ($this->parameters as $parameter) {
            $name         = $parameter['name'];
            $description  = $parameter['description'];
            $example      = $parameter['example'];
            $parameters[] = new MailTemplateParameter($name, $description, $example);
        }
        return $parameters;
    }

    /**
     * @param array<string> $recipients
     */
    public function update(string $subject, array $recipients, string $preheader, string $content) : void
    {
        $this->subject    = $subject;
        $this->recipients = $recipients;
        $this->preheader  = $preheader;
        $this->content    = $content;
    }
}
