<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Doctrine\Mail;

use Skadmin\Mailing\Model\MailParameterValue;
use SkadminUtils\DoctrineTraits\Entity;
use DateTime;
use DateTimeInterface;

/**
 * Class Role
 *
 * @Doctrine\ORM\Mapping\Entity
 */
class MailQueue
{
    public const STATUS_NEW         = 0;
    public const STATUS_IN_PROGRESS = 10;
    public const STATUS_SENT        = 20;
    public const STATUS_ERROR       = 30;

    use Entity\Id;
    use Entity\Status;

    /**
     * @Doctrine\ORM\Mapping\Column(type="text")
     * @var string
     */
    private $content;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", nullable=true)
     * @var DateTimeInterface
     */
    private $createdAt;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", nullable=true)
     * @var ?DateTimeInterface
     */
    private $sentAt = null;

    /**
     * @Doctrine\ORM\Mapping\Column(type="datetime", nullable=true)
     * @var ?DateTimeInterface
     */
    private $errorAt = null;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string")
     * @var string
     */
    private $error = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="array")
     * @var mixed[]
     */
    private $recipients;

    /**
     * @Doctrine\ORM\Mapping\Column(type="array")
     * @var mixed[]
     */
    private $parameters = [];

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="MailTemplate", cascade={"persist"})
     * @Doctrine\ORM\Mapping\JoinColumn(onDelete="cascade")
     * @var MailTemplate
     */
    private $template;

    public function __construct()
    {
        $this->status    = self::STATUS_NEW;
        $this->createdAt = new DateTime();
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function getCreatedAt() : DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getSentAt() : ?DateTimeInterface
    {
        return $this->sentAt;
    }

    public function getErrorAt() : ?DateTimeInterface
    {
        return $this->errorAt;
    }

    public function getError() : string
    {
        return $this->error;
    }

    /**
     * @return string[]
     */
    public function getRecipients() : array
    {
        return $this->recipients;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameters() : array
    {
        $parameters = [];
        foreach ($this->parameters as $parameter) {
            $parameters[] = new MailParameterValue($parameter['name'], $parameter['value']);
        }
        return $parameters;
    }

    /**
     * @param string[] $recipients
     * @param MailParameterValue[] $parameterValues
     */
    public function create(MailTemplate $template, array $recipients, array $parameterValues, string $content) : void
    {
        $this->template   = $template;
        $this->recipients = $recipients;
        $this->content    = $content;

        $parameters = [];
        foreach ($parameterValues as $parameterValue) {
            $parameters[] = $parameterValue->getDataForSerialize();
        }

        $this->parameters = $parameters;
    }

    public function setStatusSent() : void
    {
        $this->status  = self::STATUS_SENT;
        $this->sentAt  = new DateTime();
        $this->errorAt = null;
        $this->error   = '';
    }

    public function setStatusInProgress() : void
    {
        $this->status = self::STATUS_IN_PROGRESS;
    }

    public function setStatusError(string $error) : void
    {
        $this->status  = self::STATUS_ERROR;
        $this->errorAt = new DateTime();
        $this->error   = $error;
    }

    public function getTemplate() : MailTemplate
    {
        return $this->template;
    }
}
