<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Components\Admin;

interface IOverviewFactory
{
    public function create() : Overview;
}
