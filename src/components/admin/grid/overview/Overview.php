<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Components\Admin;

use App\Components\Grid\GridControl;
use App\Components\Grid\GridDoctrine;
use App\Model\Doctrine\Role\Privilege;
use App\Model\System\ABaseControl;
use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Mailing\BaseControl;
use Skadmin\Mailing\Doctrine\Mail\MailFacade;
use Skadmin\Mailing\Doctrine\Mail\MailTemplate;
use Skadmin\Translator\Translator;
use function sprintf;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    /** @var MailFacade */
    private $facade;

    public function __construct(MailFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'mailing.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('subject', 'mailing.overview.subject')
            ->setRenderer(function (MailTemplate $mailTemplate) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $mailTemplate->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($mailTemplate->getSubject());

                return $name;
            });

        // FILTER
        $grid->addFilterText('subject', 'mailing.overview.subject')
            ->setSplitWordsSearch(false);

        // IF USER ALLOWED WRITE
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'mailing.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        return $grid;
    }
}
