<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use Skadmin\Mailing\Doctrine\Mail\MailFacade;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use App\Model\System\Strings as SystemStrings;
use DateTimeInterface;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Mailing\Doctrine\Mail\MailTemplate;
use Skadmin\Translator\Translator;
use Throwable;
use Ublaboo\Mailing\MailFactory;
use function method_exists;
use function sprintf;
use function trim;

class MailService
{
    /** @var MailFactory */
    private $mailFactory;

    /** @var MailFacade */
    private $facade;

    /** @var FileStorage */
    private $fileStorage;

    /** @var Translator */
    private $translator;

    public function __construct(MailFactory $mailFactory, MailFacade $facade, FileStorage $fileStorage, Translator $translator)
    {
        $this->mailFactory = $mailFactory;
        $this->facade      = $facade;
        $this->fileStorage = $fileStorage;
        $this->translator  = $translator;
    }

    /**
     * @param string[] $recipients
     */
    public function addByTemplateType(string $type, CMail $cMail, array $recipients, bool $send = false) : ?MailQueue
    {
        return $this->add($this->getTemplateByType($type), $cMail, $recipients, $send);
    }

    /**
     * @param string[] $recipients
     */
    public function add(MailTemplate $template, CMail $cMail, array $recipients, bool $send = false) : ?MailQueue
    {
        $content = Strings::replace($template->getContent(), '~\[[a-z\-]+\]~i', static function (array $m) use ($cMail) : string {
            $match  = trim($m[0], '[]');
            $method = sprintf('get%s', SystemStrings::camelize($match));

            if (method_exists($cMail, $method)) {
                $value = $cMail->$method();

                if ($value instanceof DateTimeInterface) {
                    return $value->format('d.m.Y H:i');
                }

                return (string) $value;
            }

            return $match;
        });

        $mailQueue = $this->facade->createMailQueue($template, $cMail, $recipients, $content);

        if ($send) {
            $mailQueue = $this->send($mailQueue);
        }

        return $mailQueue;
    }

    public function resendFailedEmails() : void
    {
        foreach ($this->facade->findFailedEmails() as $mailQueue) {
            $this->send($mailQueue);
        }
    }

    public function send(MailQueue $mailQueue) : ?MailQueue
    {
        $this->facade->prepareMail($mailQueue);
        if (! $mailQueue->isLoaded()) {
            return null;
        }

        $mailTemplate = $mailQueue->getTemplate();
        $cMail        = new CMailMessage($mailTemplate->getSubject(), $mailQueue->getContent(), $mailTemplate->getPreheader(), $mailQueue->getRecipients(), $mailTemplate->getRecipients());

        $attachments = [];
        foreach ($mailQueue->getParameters() as $parameter) {
            if ($parameter->getName() === 'attachments') {
                foreach (explode(';', $parameter->getValue()) as $fileIdentifier) {
                    if ($fileIdentifier !== '') {
                        $attachments[] = $this->fileStorage->getFilePreview($fileIdentifier);
                    }
                }
            } elseif ($parameter->getName() === 'email-to') {
                if (trim($parameter->getValue()) !== '') {
                    $cMail->addRecipient($parameter->getValue());
                }
            }

        }
        $cMail->setAttachments($attachments);

        try {
            $this->mailFactory->createByType('Skadmin\Mailing\Model\MailMessage', $cMail)
                ->send();
            $mailQueue = $this->facade->sendMail($mailQueue);
        } catch (Throwable $e) {
            $mailQueue = $this->facade->failMail($mailQueue, $e->getMessage());
        }

        return $mailQueue;
    }

    public function getTemplateByType(string $type) : MailTemplate
    {
        /** @var MailTemplate $mailTemplate */
        $mailTemplate = $this->facade->findByType($type);
        return $mailTemplate;
    }

    public function sendByMailQueueId(int $id) : ?MailQueue
    {
        return $this->send($this->facade->getQueue($id));
    }
}
