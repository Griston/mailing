<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use Nette\SmartObject;

class MailParameterValue
{
    use SmartObject;

    /** @var string */
    private $name;

    /** @var mixed */
    private $value;

    /**
     * @param mixed $value
     */
    public function __construct(string $name, $value)
    {
        $this->name  = $name;
        $this->value = $value;
    }

    /**
     * @return string[]
     */
    public function getDataForSerialize() : array
    {
        return [
            'name'  => $this->getName(),
            'value' => $this->getValue(),
        ];
    }

    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
