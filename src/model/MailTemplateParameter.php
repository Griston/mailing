<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use Nette\SmartObject;

class MailTemplateParameter
{
    use SmartObject;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var string */
    private $example;

    public function __construct(string $name, string $description = '', string $example = '')
    {
        $this->name        = $name;
        $this->description = $description;
        $this->example     = $example;
    }

    /**
     * @return string[]
     */
    public function getDataForSerialize() : array
    {
        return [
            'name'        => $this->getName(),
            'description' => $this->getDescription(),
            'example'     => $this->getExample(),
        ];
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getExample() : string
    {
        return $this->example;
    }
}
