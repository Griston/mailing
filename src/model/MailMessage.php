<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use Nette\Mail\Message;
use Nette\Utils\Validators;
use Ublaboo\Mailing\AbstractMail;
use Ublaboo\Mailing\IComposableMail;
use Ublaboo\Mailing\IMessageData;
use function count;
use function is_array;

class MailMessage extends AbstractMail implements IComposableMail
{
    public function compose(Message $message, ?IMessageData $_mailData) : void
    {
        $this->template->layout = $this->mailAddresses['defaultLayout'];

        $message->setFrom($this->mailAddresses['defaultSenderEmail'], $this->mailAddresses['defaultSenderName']);

        if (is_array($this->mailAddresses['defaultRecipient'])) {
            $recipients = $this->mailAddresses['defaultRecipient'];
        } else {
            $recipients = [$this->mailAddresses['defaultRecipient']];
        }

        foreach ($recipients as $recipient) {
            if (! Validators::isEmail($recipient)) {
                continue;
            }

            $message->addBcc($recipient);
        }

        /** @var CMailMessage $mailData */
        $mailData = $_mailData;

        $message->setSubject($mailData->getSubject());

        foreach ($mailData->getSystemRecipients() as $recipient) {
            if (! Validators::isEmail($recipient)) {
                continue;
            }

            $message->addBcc($recipient);
        }

        // Pokud je pouze jeden adresát, tak ho nastavíme přes addTo, pokud jich je více, tak přes addBcc
        if (! $this->mailAddresses['isTest']) {
            $recipients = $mailData->getRecipients();

            if (count($recipients) === 1) {
                $recipient = $recipients[0];
                if (Validators::isEmail($recipient)) {
                    $message->addTo($recipient);
                }
            } else {
                foreach ($mailData->getRecipients() as $recipient) {
                    if (! Validators::isEmail($recipient)) {
                        continue;
                    }

                    $message->addBcc($recipient);
                }
            }
        }

        $this->template->title     = $mailData->getSubject();
        $this->template->content   = $mailData->getContent();
        $this->template->preheader = $mailData->getPreheader();

        foreach ($mailData->getAttachments() as $attachment) {
            $message->addAttachment($attachment->getPath());
        }
    }
}
