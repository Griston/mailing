<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use App\Model\System\Strings;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

abstract class CMail
{
    /**
     * @return mixed[]
     */
    abstract public static function getModelForSerialize() : array;

    /**
     * @return MailParameterValue[]
     */
    abstract public function getParameterValues() : array;

    /**
     * @return string[]
     *
     * @throws ReflectionException
     */
    protected static function getProperties(string $class) : array
    {
        $reflection  = new ReflectionClass($class);
        $_properties = $reflection->getProperties(ReflectionProperty::IS_PRIVATE);

        $properties = [];

        /** @var ReflectionProperty $property */
        foreach ($_properties as $property) {
            if ($property->getDeclaringClass()->getName() !== $class) {
                continue;
            }

            $properties[] = Strings::camelizeToWebalize($property->getName());
        }

        return $properties;
    }
}
