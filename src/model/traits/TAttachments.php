<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model\Traits;

trait TAttachments
{
    /** @var FilePreview[] */
    private $attachments;

    /**
     * @return FilePreview[]
     */
    public function getAttachments() : array
    {
        return $this->attachments;
    }

    /**
     * @param FilePreview[] $attachments
     */
    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }
}
