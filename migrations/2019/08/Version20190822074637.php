<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190822074637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mail_queue (id INT AUTO_INCREMENT NOT NULL, template_id INT DEFAULT NULL, content LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, sent_at DATETIME DEFAULT NULL, error_at DATETIME DEFAULT NULL, error VARCHAR(255) NOT NULL, recipients LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', parameters LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', status INT NOT NULL, INDEX IDX_4B3EDD0C5DA0FB8 (template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mail_queue ADD CONSTRAINT FK_4B3EDD0C5DA0FB8 FOREIGN KEY (template_id) REFERENCES mail_template (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE mail_queue');
    }
}
