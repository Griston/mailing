<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\Mailing\BaseControl;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510161610 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'mailing',
            'name'         => 'Mailing',
            'is_active'    => 1,
        ];

        $this->addSql(
            'INSERT INTO package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resources = [
            [
                'name'        => BaseControl::RESOURCE,
                'title'       => sprintf('role-resource.%s.title', BaseControl::RESOURCE),
                'description' => sprintf('role-resource.%s.description', BaseControl::RESOURCE),
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }

        $this->addSql('DELETE FROM resource WHERE name = :name', ['name' => 'mail']);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
