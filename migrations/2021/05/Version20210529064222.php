<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210529064222 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'mailing.overview.title', 'hash' => 'c358e9e51d773a7ed976f6351761be46', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-maily|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.overview.subject', 'hash' => '58d93b2b16eca83b071bc04cf6ace27e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Předmět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.overview.action.edit', 'hash' => '70720aa85a207c81a825fbcd8a917dd7', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.title - %s', 'hash' => '22be9e07fa135ce5535ae5492d5d0327', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace e-mailu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.subject', 'hash' => 'e43762b172a3e0e1da591f120d5d2f66', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Předmět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.recipients', 'hash' => '74215119414e2af12ed0830cd0c84a7b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Příjemci', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.preheader', 'hash' => 'bce828b4e73dc86ddd4ca7bc658ee8a6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Preheader', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.preheader.title', 'hash' => 'eec1e2a75bf7092129b804c4547e893a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Preheader', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.preheader.title-content %s', 'hash' => '067d95c39009d404a52e11ce6a57c5ee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Preheader je text pod/za předmětem e-mailu, který uvidíte, když se podíváte do svého e-mailového klienta. %s Po předmětu e-mailu je to nejdůležitějších 50 znaků, které rozhodnou o tom, jestli příjemce váš newsletter otevře nebo smaže.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.content', 'hash' => 'eea3b278fca1efe913d22aff320aeadc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.help', 'hash' => 'f0330b05fe1cc6077d276c1a02d71448', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nápověda', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.table.help', 'hash' => '303b7704fbe3ed487efaf5c3d64eb124', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zástupné znaky můžete vložit do textu a před odesláním budou nahrazeny skutečnými hodnotami. Pokud kliknete na buňku se zástupným znakem, automaticky se Vám zkopíruje do paměti (ctrl+c).', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.table.name', 'hash' => '140338dc80c54a82c3a7df5a3ad01a9c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zástupný znak', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.table.description', 'hash' => '5aa40b9aaeab426bb54bcdcd5c502327', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.table.example', 'hash' => '8e8a2b7b41a953bc817dd041f30c776c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Příklad použití', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.send', 'hash' => '158f08d59c8807b74946509ada696afe', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.send-back', 'hash' => '16bed7a91b39611e1ad8a0d9d225044b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.back', 'hash' => '2c0c86429f6752d4c05c994dc85f15c4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mailing.form.edit.flash.success', 'hash' => '4d7349f575468fe6c9ad37adb4eb2882', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Šablona e-mailu byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
